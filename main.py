import pandas as pd
import os
import glob
import pathlib
import shutil
import cx_Oracle
from datetime import datetime
import string    
import random
import utility
import update
import conf
from configparser import ConfigParser

file_config = conf.fileprocessconfig()
IN_dir_path = os.getcwd()+ '/' + file_config['files_in']
ERR_dir_path = os.getcwd()+ '/' + file_config['files_err']
PROCESSED_dir_path = os.getcwd()+ '/' + file_config['files_processed']

files = os.listdir(os.getcwd()+"/files/IN")

for f in files:
    try:
        con = cx_Oracle.connect('mfx_training/mfx_training@mercuryfx.chwkrfaqj9m1.ap-south-1.rds.amazonaws.com:1521/orcl')
        cursor = con.cursor()
        print(con)
        date = datetime.today()
        filename = f
        ran = ''.join(random.choices(string.ascii_uppercase + string.digits, k = 10))  
        q = "INSERT INTO mfx_itr_upl_log (log_id,file_name,uploaded_by,upload_date,upload_status,upload_error,batch_no,total_record) VALUES(:log_id, :file_name, :uploaded_by, :upload_date, :upload_status, :upload_error,:batch_no,:total_record)"
        cursor.execute(q, log_id=ran, file_name=f, uploaded_by="Admin", upload_date=datetime.today(), upload_status="null", upload_error="null",batch_no="null",total_record="10002")

        con.commit()
        print('Record inserted successfully')

    except cx_Oracle.DatabaseError as e:
        print("There is a problem with Oracle", e)
    finally:
        if cursor:
            cursor.close()
        if con:
            con.close()
    if pathlib.Path(f).suffix == ".xlsx":
        df = pd.read_excel(IN_dir_path+'/'+f, skiprows=4)
        val = utility.getheaderslist(list(df.columns.values))
        print(val)

        try:
            con = cx_Oracle.connect('mfx_training/mfx_training@mercuryfx.chwkrfaqj9m1.ap-south-1.rds.amazonaws.com:1521/orcl')
            print(con)
            cursor = con.cursor()
            sql = 'SELECT * from mfx_itr_upl_log where file_name = :1 '
            cursor.execute(sql,[f])
            row = cursor.fetchall()

            if len(row) > 1:
                print("file alreay exist")
                update.update('E','duplicate file',ran)
            elif val == "false":
                update.update('E',"INvalid file",ran)
                shutil.move(IN_dir_path+'/'+f,ERR_dir_path+'/'+f)

            else:
                try:
                    print("con while inserting data",con)
                    df = pd.read_excel(IN_dir_path+'/'+f, skiprows=4)
                    data = df.values.tolist()
                    print("data",data)

            
                    cur = con.cursor()
                    cur.executemany('insert into mfx_itr_upl_data values(:record_no,:pan,:name,:pan_allotment_date,:pan_aadhaar_link_status,:specified_person_206ab_206cca)', data)
                    con.commit()
                except cx_Oracle.DatabaseError as er:
                    print('There is an error in Oracle database:', er)
                    
            
                except Exception as er:
                    update.update('E',er,ran)
                    shutil.move(IN_dir_path+'/'+f,ERR_dir_path+'/'+f)
                else:
                    update.update('Y',er,ran)
                    shutil.move(IN_dir_path+'/'+f,PROCESSED_dir_path+'/'+f)

                finally:
                    if cursor:
                        cursor.close()
                    if con:
                        con.close()


        except cx_Oracle.DatabaseError as e:
            print("There is a problem with Oracle", e)

    else:
        print("file in else",f)
        shutil.move(IN_dir_path+'/'+f,ERR_dir_path+'/'+f)

        
        

