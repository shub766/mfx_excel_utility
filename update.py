import cx_Oracle
from datetime import datetime
import random
import string

def update(upload_status,upload_error,log_id):
    # print("IN update")
    # print(upload_status)
    # print(str(upload_error))

    print(log_id)
    try:
        con = cx_Oracle.connect('mfx_training/mfx_training@mercuryfx.chwkrfaqj9m1.ap-south-1.rds.amazonaws.com:1521/orcl')
        cursor = con.cursor()
        sql = 'update  mfx_itr_upl_log set upload_status=:1,upload_error= :2 where log_id = :3'
        # cursor.execute(sql,('MUMS86155G.TV919_Compliance_Check_206AB_206CCA_22-06-2021_1.xlsx','satyam'))
        cursor.execute(sql,[upload_status,str(upload_error),log_id])
        print('Record inserted successfully')
        con.commit()

    except cx_Oracle.DatabaseError as e:
        print("There is a problem with Oracle", e)

        # by writing finally if any error occurs
        # then also we can close the all database operation
    finally:
        if cursor:
            cursor.close()
        if con:
            con.close()



def insert(f):
    print("IN insert")
    try:
        con = cx_Oracle.connect('mfx_training/mfx_training@mercuryfx.chwkrfaqj9m1.ap-south-1.rds.amazonaws.com:1521/orcl')
        cursor = con.cursor()
        print(con)
        date = datetime.today()
        filename = f
        ran = ''.join(random.choices(string.ascii_uppercase + string.digits, k = 10))  
        q = "INSERT INTO mfx_itr_upl_log (log_id,file_name,uploaded_by,upload_date,upload_status,upload_error,batch_no,total_record) VALUES(:log_id, :file_name, :uploaded_by, :upload_date, :upload_status, :upload_error,:batch_no,:total_record)"
        cursor.execute(q, log_id=ran, file_name=f, uploaded_by="Admin", upload_date=datetime.today(), upload_status="null", upload_error="null",batch_no="null",total_record="10002")

        con.commit()
        print('Record inserted successfully')

    except cx_Oracle.DatabaseError as e:
        print("There is a problem with Oracle", e)

        # by writing finally if any error occurs
        # then also we can close the all database operation
    finally:
        if cursor:
            cursor.close()
        if con:
            con.close()