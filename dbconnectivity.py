import cx_Oracle
import pandas as pd
try:
    con = cx_Oracle.connect('mfx_training/mfx_training@mercuryfx.chwkrfaqj9m1.ap-south-1.rds.amazonaws.com:1521/orcl')
    print(con)
    cursor = con.cursor()
    df = pd.read_excel('./files/IN/MUMS86155G.TV919_Compliance_Check_206AB_206CCA_22-06-2021_1.xlsx', skiprows=4)
    data = df.values.tolist()
    print(data)
    cur = con.cursor()
    cur.executemany('insert into mfx_itr_upl_data values(:record_no,:pan,:name,:pan_allotment_date,:pan_aadhaar_link_status,:specified_person_206ab_206cca)', data)
    con.commit()
    # sql = 'SELECT * from mfx_itr_upl_log where file_name = :1 '
    # cursor.execute(sql,['MUMS86155G.TV919_Compliance_Check_206AB_206CCA_22-06-2021_1.xlsx'])
    # row = cursor.fetchone()
    # print(type(row))
    # print('Record inserted successfully')

except cx_Oracle.DatabaseError as e:
    print("There is a problem with Oracle", e)

    # by writing finally if any error occurs
    # then also we can close the all database operation
finally:
    if cursor:
        cursor.close()
    if con:
        con.close()